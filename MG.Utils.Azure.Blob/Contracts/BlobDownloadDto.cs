﻿using System.IO;
using Azure.Storage.Blobs.Models;

namespace MG.Utils.Azure.Blob.Contracts
{
    public class BlobDownloadDto
    {
        public BlobDownloadDto(BlobDownloadInfo downloadInfo, string fileName)
        {
            Content = downloadInfo.Content;
            ContentType = downloadInfo.ContentType;
            FileName = fileName;
            ContentLength = downloadInfo.ContentLength;
        }

        public BlobDownloadDto(
            Stream stream,
            string blobName,
            string contentType = "application/pdf")
        {
            Content = stream;
            FileName = blobName;
            ContentType = contentType;
            ContentLength = stream.Length;
        }

        public BlobDownloadDto(
            BlobDownloadInfo downloadInfo,
            string fileName,
            string contentType)
            : this(
                downloadInfo.Content,
                fileName,
                contentType)
        {
        }

        public Stream Content { get; set; }

        public string FileName { get; set; }

        public string ContentType { get; set; }

        public long ContentLength { get; set; }
    }
}