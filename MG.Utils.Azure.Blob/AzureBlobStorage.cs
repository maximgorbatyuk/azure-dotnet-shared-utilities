﻿using System.IO;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Blobs.Models;
using MG.Utils.Azure.Blob.Contracts;
using Microsoft.AspNetCore.StaticFiles;

namespace MG.Utils.Azure.Blob
{
    // https://github.com/Azure/azure-sdk-for-net/blob/Azure.Storage.Blobs_12.8.0/sdk/storage/Azure.Storage.Blobs/README.md
    // https://docs.microsoft.com/en-us/azure/storage/blobs/storage-quickstart-blobs-dotnet
    public class AzureBlobStorage : IBlobStorage
    {
        private readonly BlobStorageSettings _settings;

        public AzureBlobStorage(BlobStorageSettings settings)
        {
            _settings = settings;
        }

        public async Task<BlobUploadDto> UploadAsync(string blobName, Stream data)
        {
            var client = await _settings.FilesContainerAsync();
            var response = await client.UploadBlobAsync(blobName, data);

            return new BlobUploadDto(response.GetRawResponse().Status, blobName);
        }

        public async Task<BlobDownloadDto> DownloadAsync(string blobName)
        {
            var client = await _settings.FilesContainerAsync();

            Response<BlobDownloadInfo> data = await client.GetBlobClient(blobName).DownloadAsync();

            const string defaultContentType = "application/octet-stream";
            if (!new FileExtensionContentTypeProvider().TryGetContentType(blobName, out var contentType))
            {
                contentType = defaultContentType;
            }

            return new BlobDownloadDto(downloadInfo: data.Value, fileName: blobName, contentType: contentType);
        }

        public async Task<bool> DeleteAsync(string blobName)
        {
            var client = await _settings.FilesContainerAsync();
            return await client.GetBlobClient(blobName).DeleteIfExistsAsync();
        }
    }
}